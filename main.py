# main.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
from numpy.random import RandomState

from plot import plot_pops, load_rcparams
from reference_vectors import create_reference_vectors
from normalize import norm_matrix
from expansion import adapt_weights
from associate import associate

if __name__ == "__main__":
    
    # load objs
    objs = np.genfromtxt("objs.txt")
    
    # create weights
    m_objs = 3
    h = 12
    weights = create_reference_vectors(m_objs, h)
    
    # estimate vectors
    z_ideal = objs.min(axis=0)
    z_nadir = objs.max(axis=0)
    
    # normalize
    norm_objs = norm_matrix(objs, z_ideal, z_nadir)
    
    # associate individuals and weight vectors
    rho, pie = associate(norm_objs, weights, criteria="pdist")

    # identify crowded weight vectors
    # ie, those vectors with density > 0
    central_ids = np.where(rho > 0)[0]
    
    # or select just a few of them
    central_ids = central_ids[[2, 25, 10]]
    
    # create new weight vectors around central vectors
    new_weights = adapt_weights(central_ids, weights.copy(), h, scale=0.5)
    
    # plot
    pops = (weights, objs, weights[central_ids], new_weights)
    lbls = ("weights", "objs", "central vectors", "new weights")
    load_rcparams((10, 8))
    plot_pops(pops, lbls, lower_lims=(0,0,0), upper_lims=(1.1, 1.1, 1.1), ncol=1)
