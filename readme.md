# Uniform creation of weight vectors

This repository contains a python implementation of the method described in [Jain13,Deb14] for adapting weight vectors. The weight adaptation method comprises two steps:

- Identification of crowded weight vectors.
- Addition of vectors around crowded weight vectors.



## Example

To illustrate the weight adaptation method

```
python3 main.py
```

This script creates this figure:

![](figure.png)

New weight vectors are added around three central vectors. Here is the code. First, load the objective vectors:

```python
# load objs
objs = np.genfromtxt("objs.txt")
```

Then, create the weight vectors:

```python
# create weights
m_objs = 3
h = 12
weights = create_reference_vectors(m_objs, h)
```

Next, normalize the objective vectors:

```python
# estimate vectors
z_ideal = objs.min(axis=0)
z_nadir = objs.max(axis=0)

# normalize
norm_objs = norm_matrix(objs, z_ideal, z_nadir)
```

Now, associate solutions and weight vectors using perpendicular distance:

```python
# associate individuals and weight vectors
rho, pie = associate(norm_objs, weights, criteria="pdist")
```

Identify crowded vectors as follows:

```python
# identify crowded weight vectors
# ie, those vectors with density > 0
central_ids = np.where(rho > 0)[0]
```

and create new vectors around them:

```python
# create new weight vectors around central vectors
new_weights = adapt_weights(central_ids, weights.copy(), h, scale=0.5)    
```





## References

- [Jain13] *An Improved Adaptive Approach for Elitist Nondominated Sorting Genetic Algorithm for Many-Objective Optimization*.
- [Deb14] *An Evolutionary Many-Objective Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach*.





## TODO

- [ ] Copiar `associate.py`, `normalize.py`, `expansion.py` de `mdp/methods/amoead_mdp_wa` en este repositorio.
- [ ] Crear `main.py` para el ejemplo
- [ ] Describir ejemplo en `readme.md`
- [ ] Revisar `labs/amoead/short_paper_emo19` para generar las figuras
- [ ] Revisar el repo `density_test`.



```
# Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:auraham/test.git
git push -u origin --all
git push -u origin --tags
```

